#Komnetari
print("Ja sam Marko") #stampa Ja sam Marko

#Inline comment primjer

'''
Ovo je multiline comment
moze i ovo
i ovo
'''

print(2.0+5)
print(10.000-5)
print(40*30*0.05)

#exponent
print(2**5)

#division
print(2/4)
print(23/5)
print(45.0/5)

print(23//5) #zaokruzili smo
#modul

print(25%4)

#a = 2 + 5
#b = 3.3 * 3
#c = 10.0 / 25



print(25*15+33/2.0)
a = 25-15
b = 33/2.0
print(a+b)

#Binary number operation

print(1<<3)

print(256 & 255) #and
print(256 | 255) #or
print(60 ^ 13) #xor


#strings

string = 'Ja sam Marko'

print(string[5:11])
print(string[:2])
print(string[7:])

string2 = 'Mar'+'ko'
print(string2)

string3 = 100 * 'MARKO '
print(string3)

print("Danas sam imao {0} solje {1}". format(3, "kafe")) #format metoda se koristi
print("Cijene: ({x}, {y}, {z})".format(x=2.0, y = 1.25, z = 5))
print("{auto} je imalo {0} sudara u toku prethodnih {1} mjeseci".format(5,6, auto='auto'))
print("{:<20}".format("tekst"))
print("{:>20}".format("tekst"))

print("{:b}".format(2)) # pretvoriti broj u binarni zapis
print("{:x}".format(2566)) #pretvoriti broj u heksadecimanli zapis
print("{:o}".format(457994459)) #pretvoriti broj u oktalni zapis

#Specificni karakteri

#kako stampati znake navodnika
"Ja sam mil'ov mali" #i obratno double u single znaci navodnika

print(r'c:\number\nan')
print()

print("""\
    Zdravo
            Evo moze i u vise 
                redova da se stampa
        """)




