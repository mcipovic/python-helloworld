'''
Ovdje ce da ide kod sa ovog online kursa
https://www.datacamp.com/courses/intro-to-python-for-data-science
'''


#Prvi zadatak

'''
Suppose you have $100, which you can invest with a 10% return each year.
After one year, it's 100×1.1=110 dollars, and after two years it's 100×1.1×1.1=121. 
Add code on the right to calculate how much money you end up with after 7 years.
'''

print(100*1.1**7)

#Create a variable savings with the value 100.
#savings = 100

#Check out this variable by typing print(savings) in the script.
#print(savings)

#Vjezba2

#Create variable savings
savings = 100
#Cerate variable factor
factor = 1.10

#Calculate result
result = savings*factor**7

#Print out result
print(result)

#Vjezba3

#Calculate the product of savings and factor. Store the result in year1.
year1 = savings*factor

#printing out the type of year1

print(type(year1))

#Calculate the sum of desc and desc and store the result in a new variable doubledesc.
desc = "compound interest"

doubledesc = desc + desc

print(doubledesc)

#Vjezba4
# Fix the printout

print("I  started with $" + str(savings) + " and now have $" + str(result) + ". Awesome!")

#Vjezba5
#Create a list, areas, that contains the area of the hallway (hall), kitchen (kit), living room (liv), bedroom (bed) and bathroom (bath), in this order. Use the predefined variables.
# area variables (in square meters)
hall = 11.25
kit = 18.0
liv = 20.0
bed = 10.75
bath = 9.50
#Create list areas

areas = ["hallway", hall, "kitchen", kit, "living room", liv, "bedroom", bed, "bathroom", bath]

#Print areas
print(areas)

#vjezba6
#Finish the list of lists so that it also contains the bedroom and bathroom data. Make sure you enter these in order!

# house information as list of lists

house = [["hallway", hall],
         ["kitchen", kit],
         ["living room", liv],
         ["bedroom", bed],
         ["bathroom", bath]]
# Print out house
print(house)

#Print out the type of house
print(type(house))

#vjezba7

# Print out second element from areas
print(areas[1])

# Print out last element from areas
print(areas[-1])

# Print out the area of the living room
print(areas[5])

#vjezba8

#Using a combination of list subsetting and variable assignment, create a new variable, eat_sleep_area, that contains the sum of the area of the kitchen and the area of the bedroom.

eat_sleep_area = areas[3] + areas[-3]

# Print the variable eat_sleep_area
print(eat_sleep_area)

#vjezba9
# Use slicing to create downstairs
downstairs = areas[0:6]

upstairs = areas[6:]

print(downstairs)
print(upstairs)

#vjezba10

'''
You did a miscalculation when determining the area of the bathroom; it's 10.50 square meters instead of 9.50. Can you make the changes?
'''

# Correct the bathroom area
areas[9] = 10.50

# Change "living room" to "chill zone"
areas[4] = "chill zone"
print(areas)

#vjezba11

#Use the + operator to paste the list ["poolhouse", 24.5] to the end of the areas list. Store the resulting list as areas_1.
# Add poolhouse data to areas, new list is areas_1

areas_1 = areas + ["poolhouse", 24.5]

# Add garage data to areas_1, new list is areas_2
areas_2 = areas_1 + ["garage", 15.45]

print(areas_2)

#vjezba12
