a = 5
b = 5
print(5 / 8)
print(a/b)
print(4 + 10)

#Sabiranje i oduzimanje
print(5 + 5)
print(5 - 5)

#Mnozenje i dijeljenje
print(5*5)
print(25/5)

#Modulo (https://sr.wikipedia.org/wiki/%D0%9C%D0%BE%D0%B4%D1%83%D0%BB%D0%B0%D1%80%D0%BD%D0%B0_%D0%B0%D1%80%D0%B8%D1%82%D0%BC%D0%B5%D1%82%D0%B8%D0%BA%D0%B0)
print(17%7)

#Varijable

x = 66
print(x)

#Create a variable desc
desc = "compound interest"
profitable = True

print(desc, profitable)

#Type() function testing
print(type(desc))
print(type(profitable))
print(type(a))

# Definition of pi_string
pi_string = "3.1415926"

# Convert pi_string into float: pi_float
pi_float = float(pi_string)
print(pi_float)

#Liste

a1 = "Ja"
b1 = "Ti"
c1 = 25
d1 = 50.25

moja_lista = [a1, b1, c1, d1]

print(type((moja_lista)))

# Create variables var1 and var2
var1 = [1, 2, 3, 4]
var2 = True

# Print out type of var1
print(type(var1))

# Print out length of var1
print(len(var1))
# Convert var2 to an integer: out2
out2 = int(var2); print(var2)


# Create lists first and second
first = [11.25, 18.0, 20.0]
second = [10.75, 9.50]

# Paste together first and second: full
full = first+second
# Sort full in descending order: full_sorted
full_sorted = sorted(full, reverse=False)
print(full)
print("Sortirana " + str(full_sorted)) # igrao se i dodao string na outputu da se bolje razumije

#Metode
#string metode vjezbe

# string to experiment with: room
room = "poolhouse"

# Use upper() on room: room_up
room_up = room.upper()

# Print out room and room_up
print(room)
print(room_up)

# Print out the number of o's in room
print(room.count("o"))

#list metode

x1 = [1,2,3,4,5]
x2 = x1.index(5)
x3 = x1.count(10)
print(x2); print (x3)
